FROM node:18.14-alpine AS builder

RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY . .
RUN yarn --immutable
RUN yarn build

FROM nginx:1.23-alpine AS runner
COPY --from=builder /app/out /usr/share/nginx/html