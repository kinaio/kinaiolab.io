import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Head>
        <title>kina.io</title>
        <meta name="description" content="kina.io" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div className={styles.description}>
          <div>Copyright © {new Date().getFullYear()} kina.io.</div>
        </div>

        <div className={styles.center}>
          <Image
            className={styles.logo}
            src="/kina.svg"
            alt="Kina Logo"
            width={232}
            height={68}
            priority
          />
        </div>

        <div className={styles.grid}>
          <a
            href="https://blog.kina.io/"
            className={styles.card}
            target="_blank"
            rel="noopener noreferrer"
          >
            <h2 className={inter.className}>
              Blog <span>-&gt;</span>
            </h2>
          </a>
        </div>
      </main>
    </>
  );
}
